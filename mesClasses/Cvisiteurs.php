<?php


/* ************ Classe métier Cvisiteur et Classe de contrôle Cvisiteurs **************** */

class Cvisiteur
{

    public $id;
    public $login;
    public $nom;
    public $prenom;


    function __construct($sid,$slogin,$snom,$sprenom) //s pour send param envoyé
    {

        $this->id = $sid;
        $this->login = $slogin;
        $this->nom = $snom;
        $this->prenom = $sprenom;
    }
}


class Cvisiteurs 
{
 
    private $ocollVisiteurById;
    

    public function __construct()
    {
       
                  try {
                         $strConnection = 'mysql:host=localhost;dbname=gsb'; // DSN
                         $arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"); // demande format utf-8
                         $pdo = new PDO($strConnection, 'root', '', $arrExtraParam); // Instancie la connexion
                         $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// Demande la gestion d'exception car par défaut PDO ne la propose pas 

                         /* cas n°1 avec query */

                            
                             $query = 'SELECT id,login,nom,prenom from visiteur';
                             $lesVisiteurs = $pdo->query($query);                       
                             $this->ocollVisiteur = array();
                             
                             
                             foreach ($lesVisiteurs as $unVisiteur) {
                                 
                                $ovisiteur = new Cvisiteur($unVisiteur['id'],$unVisiteur['login'],$unVisiteur['nom'],$unVisiteur['prenom']);
                                $this->ocollVisiteur[$unVisiteur['id']] = $ovisiteur;
                                
                            } 
                            $this->ocollVisiteurById = $this->ocollVisiteur;
                            
                        /* fin 1ER cas */

                      }
                  catch(PDOException $e) {
                         $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
                         die($msg);
                        }
   

    }



    function getVisiteurById($sid)
    {
        
        return $this->ocollVisiteurById[$sid];
    }
    
    

}


