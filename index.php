<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'mesClasses/Cvisiteurs.php';

        $ovisiteurs = new Cvisiteurs(); //instanciation de la classe de contrôle 
        $ovisiteur = $ovisiteurs->getVisiteurById('a17'); // Récupétation du visiteur à partir de son ID
        
        // Affichage avec un echo (Instruction qui envoie une chaîne dans le flux http
        echo 'le nom du visiteur est : ' . $ovisiteur->nom;
        ?>
        <br> <!-- Un passage à la ligne -->

        <!--Affichage  dans une zone de texte HTML-->
        Le nom du visiteur est : <input type="text" value="<?= $ovisiteur->nom; ?>" name="username">
        <br> <!-- Un passage à la ligne -->
         <!--Affichage dans une zone de texte HTML à l'aide d'un echo php-->
        <?php echo 'Le nom du visiteur est : <input type="text" value="'.$ovisiteur->nom.'" name="username">' ?>
    </body>
</html>
